var express = require('express');
var app = express();
var path = require('path');

var bodyParser = require("body-parser");
app.use(bodyParser.urlencoded({ extended: false }));

var request = require("request");

var natural = require('natural');

var mimir = require('mimir'),
    bow = mimir.bow,
    dict = mimir.dict;

var TSNE = require('tsne-js');

var JSDOM = require('jsdom').JSDOM;
// Create instance of JSDOM.
var jsdom = new JSDOM('', {runScripts: 'dangerously'});
// Get window
var window = jsdom.window;

// require anychart and anychart export modules
var anychart = require('anychart')(window);
var anychartExport = require('anychart-nodejs')(anychart);

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

// viewed at http://localhost:8080
app.get('/', function(req, res) {
    res.render(__dirname + '/index');
});

function doEverything(url, query, req, res) {
    request({
        url: url,
        json: true
    }, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            var array = new Array();
            var i;
            for (i = 0; i < body.data.length; i++) {
                //Get body (the comments) from query JSON
                var comment = JSON.stringify(body.data[i].body).toLowerCase();

                //Manual cleaning
                var removengt = comment.replace(/&gt/g,"");
                var removenewline = removengt.replace(/\\n/g," ");
                var removeself = removenewline.replace(query, "");
                var alphanumonly = removeself.replace(/[^A-Za-z0-9 ]+/g,"");
                var removeextraspaces = alphanumonly.replace(/ +(?= )/g,"");

                //Stemming, from https://github.com/NaturalNode/natural#stemmers
                natural.PorterStemmer.attach();
                var stem = removeextraspaces.tokenizeAndStem();
                //Join tokens to create text for Bag-of-Words
                array.push(stem.join(' '));
            }

            //Bag-of-Words, from https://github.com/techfort/mimir
            var texts = array, voc = dict(texts);

            var bowArray = [];
            var j;
            for (j = 0; j < array.length; j++) {
                bowArray.push(bow(array[j], voc));
            }

            var transform = [];
            for (var c1 = 0; c1 < bowArray[0].length; c1++) {
                transform[c1] = [];
                for (var c2 = 0; c2 < bowArray.length; c2++) {
                    transform[c1].push(bowArray[c2][c1]);
                }
            }

            console.log("Starting TSNE");

            let model = new TSNE({
                dim: 2,
                perplexity: 30.0,
                earlyExaggeration: 4.0,
                learningRate: 100.0,
                nIter: 1000,
                metric: 'euclidean'
            });

            model.init({
                data: transform,
                type: 'dense'
            });

            let Y = model.getOutput();

            console.log("TSNE Finished");

            //render results and send variables to frontend
            res.render(__dirname + '/query-results', {Y:Y, query:query, words:voc.words});

        } else {
            console.log(error);
        }
    });

}

app.post('/query-results', function (req, res) {
    var query = req.body.search.toLowerCase();
    var limit = req.body.limit;

    //in case of multiple queries
    var replacespace = query.replace(" ", "+");

    var url = "https://api.pushshift.io/reddit/search/comment?q=" + replacespace + "&limit=" + limit;
    console.log("URL: " + url);

    doEverything(url, replacespace, req, res);
});

var server = app.listen(8080, function () {
    console.log('Node server is running..');
});